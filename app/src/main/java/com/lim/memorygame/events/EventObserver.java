package com.lim.memorygame.events;


import com.lim.memorygame.events.engine.FlipDownCardsEvent;
import com.lim.memorygame.events.engine.GameWonEvent;
import com.lim.memorygame.events.engine.HidePairCardsEvent;
import com.lim.memorygame.events.ui.BackGameEvent;
import com.lim.memorygame.events.ui.DifficultySelectedEvent;
import com.lim.memorygame.events.ui.FlipCardEvent;
import com.lim.memorygame.events.ui.NextGameEvent;
import com.lim.memorygame.events.ui.ResetBackgroundEvent;
import com.lim.memorygame.events.ui.StartEvent;
import com.lim.memorygame.events.ui.ThemeSelectedEvent;

public interface EventObserver {

	void onEvent(FlipCardEvent event);

	void onEvent(DifficultySelectedEvent event);

	void onEvent(HidePairCardsEvent event);

	void onEvent(FlipDownCardsEvent event);

	void onEvent(StartEvent event);

	void onEvent(ThemeSelectedEvent event);

	void onEvent(GameWonEvent event);

	void onEvent(BackGameEvent event);

	void onEvent(NextGameEvent event);

	void onEvent(ResetBackgroundEvent event);

}
