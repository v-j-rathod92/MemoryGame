package com.lim.memorygame;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.lim.memorygame.common.Memory;
import com.lim.memorygame.common.Music;
import com.lim.memorygame.common.Shared;
import com.lim.memorygame.engine.Engine;
import com.lim.memorygame.engine.ScreenController;
import com.lim.memorygame.events.EventBus;
import com.lim.memorygame.events.ui.BackGameEvent;
import com.lim.memorygame.fragments.DifficultySelectFragment;
import com.lim.memorygame.fragments.GameFragment;
import com.lim.memorygame.fragments.MenuFragment;
import com.lim.memorygame.ui.PopupManager;
import com.lim.memorygame.utils.Utils;


public class MainActivity extends FragmentActivity {

	private ImageView mBackgroundImage;
	private static AdView adView;
	private static InterstitialAd mInterstitialAd;
	private static AdRequest adRequest;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Shared.context = getApplicationContext();
		Shared.engine = Engine.getInstance();
		Shared.eventBus = EventBus.getInstance();

		setContentView(R.layout.activity_main);

		adView = (AdView) findViewById(R.id.adView);
		mBackgroundImage = (ImageView) findViewById(R.id.background_image);

		Shared.activity = this;
		Shared.engine.start();
		Shared.engine.setBackgroundImageView(mBackgroundImage);

		initializeAdvertise();
		// set background
		setBackgroundImage();

		// set menu
		ScreenController.getInstance().openScreen(ScreenController.Screen.MENU);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (adView != null) {
			adView.pause();
		}

		Music.pauseBackgroundMusic();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (adView != null) {
			adView.resume();
		}

		FragmentManager fm = Shared.activity.getSupportFragmentManager();
		Fragment fragment = fm.findFragmentById(R.id.fragment_container);

		if(fragment instanceof MenuFragment || fragment instanceof DifficultySelectFragment){
			Music.resumeBackgroundMusic();
		}
	}

	@Override
	protected void onDestroy() {
		Shared.engine.stop();
		super.onDestroy();

		if (adView != null) {
			adView.destroy();
		}
	}

	@Override
	public void onBackPressed() {
		if (PopupManager.isShown()) {
			PopupManager.closePopup();
			if (ScreenController.getLastScreen() == ScreenController.Screen.GAME) {
				Shared.eventBus.notify(new BackGameEvent());
			}
		} else if (ScreenController.getInstance().onBack()) {
			super.onBackPressed();
		}
	}

	private void setBackgroundImage() {
		Bitmap bitmap = Utils.scaleDown(R.drawable.back_animals, Utils.screenWidth(), Utils.screenHeight());
		bitmap = Utils.crop(bitmap, Utils.screenHeight(), Utils.screenWidth());
		bitmap = Utils.downscaleBitmap(bitmap, 2);
		mBackgroundImage.setImageBitmap(bitmap);
	}

	private void initializeAdvertise(){
		adRequest = new AdRequest.Builder()
//				.addTestDevice("C21C8AEABCB925D5933727E23B5C2567")
				.build();

		mInterstitialAd = new InterstitialAd(this);
		mInterstitialAd.setAdUnitId(getString(R.string.admob_full_screen_ad));
		mInterstitialAd.loadAd(adRequest);
		mInterstitialAd.setAdListener(new AdListener() {
			@Override
			public void onAdClosed() {
				super.onAdClosed();

				adRequest = new AdRequest.Builder()
//						.addTestDevice("C21C8AEABCB925D5933727E23B5C2567")
						.build();
				mInterstitialAd.loadAd(adRequest);
			}
		});
	}

	public static void newClickEvent(){
		if(Memory.getTotalClick() == 5){
			showInterstitial();
			Memory.clearTotalClick();
		}
		else{
			Memory.addNewClick();
		}
	}

	public static void showInterstitial() {
		if (mInterstitialAd.isLoaded()) {
			mInterstitialAd.show();
		}
	}

	public static void showBannerAd(){
		adView.setVisibility(View.VISIBLE);
		adView.loadAd(adRequest);
	}

	public static void hideBannerAd(){
		adView.setVisibility(View.GONE);
	}
}
