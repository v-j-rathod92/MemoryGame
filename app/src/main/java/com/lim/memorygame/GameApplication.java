package com.lim.memorygame;

import android.app.Application;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.lim.memorygame.utils.FontLoader;


public class GameApplication extends Application {

    private FirebaseAnalytics firebaseAnalytics;

    @Override
    public void onCreate() {
        super.onCreate();
        FontLoader.loadFonts(this);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }
}
