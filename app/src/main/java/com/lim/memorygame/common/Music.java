package com.lim.memorygame.common;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Handler;

import com.lim.memorygame.R;

public class Music {

	public static boolean OFF = false;
	public static MediaPlayer backgroundMusic;
	public static Handler handler = new Handler();
	public static void playCorrent() {
		if (!OFF) {
			MediaPlayer mp = MediaPlayer.create(Shared.context, R.raw.succeed);
			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.reset();
					mp.release();
					mp = null;
				}

			});
			mp.start();
			mp.seekTo(1300);
		}
	}

	public static void playFailed(){
		if (!OFF) {
			MediaPlayer mp = MediaPlayer.create(Shared.context, R.raw.chtoing);
			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.reset();
					mp.release();
					mp = null;
				}
			});

			mp.start();
		}
	}

	public static void playBackgroundMusic(int start_stop) {
		// TODO
        if(!OFF){
			if(backgroundMusic == null){
				backgroundMusic = MediaPlayer.create(Shared.context, R.raw.background_music);
				backgroundMusic.setOnCompletionListener(new OnCompletionListener() {
					@Override
					public void onCompletion(MediaPlayer backgroundMusic) {
						backgroundMusic.release();
						backgroundMusic.release();
						backgroundMusic = null;
					}
				});
			}

            if(start_stop == 1 && !backgroundMusic.isPlaying()){
				backgroundMusic.start();
            }

            else if(start_stop == 2 && backgroundMusic.isPlaying()){
				backgroundMusic.release();
				backgroundMusic.release();
				backgroundMusic = null;
            }
        }
	}

	public static void showStar() {
		if (!OFF) {
			MediaPlayer mp = MediaPlayer.create(Shared.context, R.raw.star);
			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.reset();
					mp.release();
					mp = null;
				}
			});
			mp.start();
		}
	}

	public static void pauseBackgroundMusic(){
		if(backgroundMusic != null && backgroundMusic.isPlaying()){
			backgroundMusic.pause();
		}
	}

	public static void resumeBackgroundMusic(){
		if(backgroundMusic != null){
			int playedLength = backgroundMusic.getCurrentPosition();
			backgroundMusic.seekTo(playedLength);
			backgroundMusic.start();
		}
	}
}
