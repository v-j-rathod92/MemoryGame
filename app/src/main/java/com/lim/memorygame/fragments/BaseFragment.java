package com.lim.memorygame.fragments;

import android.support.v4.app.Fragment;

import com.lim.memorygame.events.EventObserver;
import com.lim.memorygame.events.engine.FlipDownCardsEvent;
import com.lim.memorygame.events.engine.GameWonEvent;
import com.lim.memorygame.events.engine.HidePairCardsEvent;
import com.lim.memorygame.events.ui.BackGameEvent;
import com.lim.memorygame.events.ui.DifficultySelectedEvent;
import com.lim.memorygame.events.ui.FlipCardEvent;
import com.lim.memorygame.events.ui.NextGameEvent;
import com.lim.memorygame.events.ui.ResetBackgroundEvent;
import com.lim.memorygame.events.ui.StartEvent;
import com.lim.memorygame.events.ui.ThemeSelectedEvent;

public class BaseFragment extends Fragment implements EventObserver {

	@Override
	public void onEvent(FlipCardEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(DifficultySelectedEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(HidePairCardsEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(FlipDownCardsEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(StartEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(ThemeSelectedEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(GameWonEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(BackGameEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(NextGameEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(ResetBackgroundEvent event) {
		throw new UnsupportedOperationException();
	}

}
